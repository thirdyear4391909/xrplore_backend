const HOTEL = require('../Model/hotelModel')
exports.uploadHotel = async (req, res, next) => {
    try {
        const newHotel = await HOTEL.create(req.body)
        res.json({ hotel: newHotel, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
exports.addhighlight = async (req, res) => {
    try {
        const { hotelID, highlight } = req.body
        const highlights = { highlight: highlight }
        if (hotelID) {
            const addedHighlight = await HOTEL.findByIdAndUpdate(
                { _id: hotelID },
                { $push: { highlights: highlights } },
                { new: true }
            )
            if (addedHighlight) {
                console.log('Highlight added successfully:', addedHighlight);
                return res.json({ data: addedHighlight, status: "success" });
            } else {
                console.error('Hotel not found');
                return res.json({ message: "hotel not found", status: "failed" });
            }
        }

    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}


exports.addingReminder = async (req, res) => {
    try {
        const { hotelID, reminder } = req.body;
        const reminders = { reminder: reminder };

        if (hotelID) {
            const addedReminder = await HOTEL.findByIdAndUpdate(
                hotelID,
                { $push: { rules: reminders } }, // Use 'rules' instead of 'rule'
                { new: true }
            );
            if (addedReminder) {
                console.log('Reminder added successfully:', addedReminder);
                return res.json({ data: addedReminder, status: "success" });
            } else {
                console.error('Hotel not found');
                return res.json({ message: "Hotel not found", status: "failed" });
            }
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

exports.updateHotelDetails = async (req, res) => {
    try {
        const { hotelID } = req.body
        const updatedHotel = await HOTEL.findByIdAndUpdate(hotelID, req.body, { new: true });
        if (updatedHotel) {
            res.json({ hotel: updatedHotel, status: "success", message: "Hotel updated successfully" });
        } else {
            res.json({ status: "failed", message: "Product not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}


exports.updateHighlight = async(req,res) =>{
    try {
        const { hotelID, highlightID } = req.body;
        const hotel = await HOTEL.findById(hotelID);
        const foundHighlight = hotel.highlights.find(highlight => highlight._id == highlightID);
        if (foundHighlight) {
            foundHighlight.highlight = req.body.highlight;
            await hotel.save();
            res.json({ hotel,meaasge : "updated succcessfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Highlight not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
}

exports.updateReminder = async(req,res) =>{
    try {
        const { hotelID, reminderID } = req.body;
        const hotel = await HOTEL.findById(hotelID);
        const foundReminder = hotel.rule.find(reminder => reminder._id == reminderID);
        if (foundReminder) {
            foundReminder.reminder = req.body.reminder;
            await hotel.save();
            res.json({ hotel,meaasge : "updated succcessfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Highlight not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
}

exports.deleteReminder = async (req, res) => {
    try {
        const { hotelID, reminderID } = req.body;
        const hotel = await HOTEL.findById(hotelID);
        const foundReminderIndex = hotel.rules.findIndex(reminder => reminder._id == reminderID);
        
        if (foundReminderIndex !== -1) {
            hotel.rules.splice(foundReminderIndex, 1); 
            await hotel.save();
            res.json({ hotel, message: "Reminder deleted successfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Reminder not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
}

exports.deleteHighlight = async (req, res) => {
    try {
        const { hotelID, highlightID } = req.body;
        const hotel = await HOTEL.findById(hotelID);
        const foundHighlightIndex = hotel.highlights.findIndex(highlight => highlight._id == highlightID);
        
        if (foundHighlightIndex !== -1) {
            hotel.highlights.splice(foundHighlightIndex, 1); 
            await hotel.save();
            res.json({ hotel, message: "Highlight deleted successfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Highlight not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
}





exports.addHotelImage = async (req, res) => {
    try {
        const { hotelID } = req.body;
        const newImage = {
            imageUrl: req.file.filename
        };

        if (!hotelID) {
            return res.json({ message: "Hotel ID is required", status: "failed" });
        }

        const addedImages = await HOTEL.findByIdAndUpdate(
            { _id: hotelID },
            { $push: { hotelImages: newImage } }, // Corrected to use hotelImages
            { new: true }
        );

        if (addedImages) {
            console.log('Hotel Image added successfully:', addedImages);
            return res.json({ data: addedImages, status: "success" });
        } else {
            console.error('Hotel not found');
            return res.json({ message: "Hotel not found", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};



exports.deleteHotelImage = async (req, res) => {
    try {
        const { hotelID, hotelimageID } = req.body;
        const hotel = await HOTEL.findById(hotelID);
        const foundHotelImageIndex = hotel.hotelimages.findIndex(image => image._id == hotelimageID);
        
        if (foundHotelImageIndex !== -1) {
            hotel.hotelimages.splice(foundHotelImageIndex, 1); 
            await hotel.save();
            res.json({ hotel, message: "Hotel image deleted successfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Hotel image not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
}



// Function to get all hotels
exports.getAllHotels = async (req, res) => {
    try {
        const hotels = await HOTEL.find({});
        if (hotels.length > 0) {
            res.json({ data: hotels, status: "success" });
        } else {
            res.json({ message: "No hotels found", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


exports.getAllHotelsOFParticularUser = async (req, res) => {
    const { ownerId } = req.params; // Assuming ownerId is passed as a route parameter

    try {
        const hotels = await HOTEL.find({ owner: ownerId });

        if (hotels.length > 0) {
            res.json({ data: hotels, status: "success" });
        } else {
            res.json({ message: "No hotels found for this owner", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};





exports.addRoomType = async (req, res) => {
    try {
        const { hotelId, roomType } = req.body;
        
        // Find the hotel by ID
        const hotel = await HOTEL.findById(hotelId);
        if (!hotel) {
            return res.json({ status: "error", message: "Hotel not found" });
        }
        hotel.roomtype.push(roomType);
        await hotel.save();
        return res.status(200).json({ success: true, message: "Room type added successfully", hotel });
    } catch (error) {
        return res.status(500).json({ success: false, message: error.message });
    }
};





