const HotelBooking = require("./../Model/HotelBooking")
exports.bookHotel = async (req, res, next) => {
    try {
        const { ownerAddress, nftID, eventName, location, dateTime, termsConditions, attractionOwner, startDate, endDate, showStatus } = req.body;
        const newBooking = new HotelBooking({
            ownerAddress,
            nftID,
            eventName,
            location,
            dateTime,
            termsConditions,
            attractionOwner,
            startDate,
            endDate,
            showStatus
        });
        await newBooking.save();
        res.json({ booking: newBooking, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.getBookingDates = async (req, res) => {
    try {
        // Fetch only startDate and endDate fields from the database
        const bookings = await HotelBooking.find({}, 'startDate endDate -_id');
        const dates = bookings.map(booking => ({
            startDate: booking.startDate,
            endDate: booking.endDate
        }));
        res.json({ dates, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};