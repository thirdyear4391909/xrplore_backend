const ATTRACTION = require('../Model/attractionModal')
exports.uploadAttraction = async (req, res, next) => {
    try {
        const newattraction = await ATTRACTION.create(req.body)
        res.json({ attraction: newattraction, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}


exports.addhighlight = async (req, res) => {
    try {
        const { attractionID, highlight } = req.body
        const highlights = { highlight: highlight }
        if (!attractionID) {
            return res.json({ message: "Attraction not found", status: "failed" });
        }
        const addedHighlight = await ATTRACTION.findByIdAndUpdate(
            { _id: attractionID },
            { $push: { highlights: highlights } },
            { new: true }
        )
        if (addedHighlight) {
            console.log('Highlight added successfully:', addedHighlight);
            return res.json({ data: addedHighlight, status: "success" });
        } else {
            console.error('Attraction not found');
            return res.json({ message: "Attraction not found", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
}


exports.getAllAttractionOfSameType = async (req, res) => {
    try {
        const { attractiontype } = req.params; // Assuming attractiontype is passed as a query parameter
        const attractions = await ATTRACTION.find({ attractiontype });
        if (attractions.length > 0) {
            res.json({ data: attractions, status: "success" });
        } else {
            res.json({ message: "No attractions found", status: "failed" });
        }
        return
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


exports.getAllAttractions = async (req, res) => {
    try {
        const attractions = await ATTRACTION.find({});
        if (attractions.length > 0) {
            res.json({ data: attractions, status: "success" });
        } else {
            res.json({ message: "No attractions found", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};


exports.getAllAttractionsByUserID = async (req, res) => {
    try {
        const {owner} = req.params
        const attractions = await ATTRACTION.find({owner : owner});
        if (attractions.length > 0) {
            res.json({ data: attractions, status: "success" });
        } else {
            res.json({ message: "No attractions found", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};





exports.addAttractionImage = async (req, res) => {
    try {
        const { attractionID } = req.body;
        const newImage = {
            imageUrl: req.file.filename
        };
        if (!attractionID) {
            return res.json({ message: "Attraction ID is required", status: "failed" });
        }
        const addedImages = await ATTRACTION.findByIdAndUpdate(
            attractionID,
            { $push: { attractionimages: newImage } }, // Correct field name is used here
            { new: true }
        );
        if (addedImages) {
            console.log('Attraction Image added successfully:', addedImages);
            return res.json({ data: addedImages, status: "success" });
        } else {
            console.error('Attraction not found');
            return res.json({ message: "Attraction not found", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};





exports.updateAttractionDetails = async (req, res) => {
    try {
        const { attractionID } = req.params; // Assuming attractionID is passed in the request body
        const updatedAttraction = await ATTRACTION.findByIdAndUpdate(attractionID, req.body, { new: true });
        if (updatedAttraction) {
            res.json({ attraction: updatedAttraction, status: "success", message: "Attraction updated successfully" });
        } else {
            res.json({ status: "failed", message: "Attraction not found" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};



exports.addingReminder = async (req, res) => {
    try {
        const { attractionID, reminder } = req.body; // Assuming attractionID and reminder are passed in the request body
        const newReminder = { reminder: reminder };

        if (attractionID) {
            const addedReminder = await ATTRACTION.findByIdAndUpdate(
                attractionID,
                { $push: { rule: newReminder } }, // Use 'rule' as per your schema
                { new: true }
            );

            if (addedReminder) {
                console.log('Reminder added successfully:', addedReminder);
                return res.json({ data: addedReminder, status: "success" });
            } else {
                console.error('Attraction not found');
                return res.json({ message: "Attraction not found", status: "failed" });
            }
        } else {
            return res.status(400).json({ message: "Attraction ID is required", status: "failed" });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};



exports.deleteReminder = async (req, res) => {
    try {
        const { attractionID, reminderID } = req.   body; // Assuming attractionID and reminderID are passed in the request body
        const attraction = await ATTRACTION.findById(attractionID);

        const foundReminderIndex = attraction.rule.findIndex(reminder => reminder._id == reminderID);
        
        if (foundReminderIndex !== -1) {
            attraction.rule.splice(foundReminderIndex, 1);
            await attraction.save();
            res.json({ attraction, message: "Reminder deleted successfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Reminder not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
};


exports.deleteHighlight = async (req, res) => {
    try {
        const { attractionID, highlightID } = req.body;
        const attraction = await ATTRACTION.findById(attractionID);
        
        if (!attraction) {
            return res.json({ status: "failed", message: "Attraction not found" });
        }

        const foundHighlightIndex = attraction.highlights.findIndex(highlight => highlight._id == highlightID);
        
        if (foundHighlightIndex !== -1) {
            attraction.highlights.splice(foundHighlightIndex, 1); 
            await attraction.save();
            res.json({ attraction, message: "Highlight deleted successfully", status: "success" });
        } else {
            res.json({ status: "failed", message: "Highlight not found" });
        }
    } catch (error) {
        console.error('Error:', error.message);
        res.json({ status: "failed", message: error.message });
    }
}






