const { XrplClient } = require("xrpl-client");
const { utils, derive, sign } = require("xrpl-accountlib");
const User = require('../Model/userModels');
const client = new XrplClient(process.env.xrpltestnet);
const xrpl = require('xrpl');
const pinataSDK = require("@pinata/sdk");
const NFT = require("./../Model/nftModal")
const HotelBooking = require("./../Model/HotelBooking")




const pinataApiKey = "5b3b94de622d2011f223";
const pinataApiSecret = "5a0afb5c60495b4458b22097c2e346d64bcb200a29d67e2934277afd03a5c898";
const net = "wss://s.altnet.rippletest.net:51233";
const standbyTransferFee = "10000";
const standbyFlags = "8";
const pinata = new pinataSDK(pinataApiKey, pinataApiSecret);

const generateAccount = async (secretKey, res) => {
    let account;
    if (!utils.isValidSeed(secretKey)) {
        console.log("Invalid secret");
        res.status(400).json({ status: "error", message: "Invalid secret" });
        return null;
    }
    account = derive.familySeed(secretKey);

    const data = await client.send({
        id: 1,
        command: "account_info",
        account: account.address,
        strict: true // Ensures strict validation of the account
    });
    if (data.error) {
        console.log("Error:", data.error_message);
        res.json({ status: "error", message: data.error_message });
        return null;
    }
    console.log("account", account.address);
    return account.address; // Return the account address
};

exports.addXrplAccount = async (req, res) => {
    try {
        const { id, secretKey } = req.body;
        const accountAddress = await generateAccount(secretKey, res);
        if (!accountAddress) {
            return; // Error response already sent in generateAccount
        }

        const user = await User.findById(id);
        if (!user) {
            return res.status(404).json({ error: "User not found" });
        }

        // Add the new XRPL account to the account array
        if (!user.account) {
            user.account = [];
        }
        user.account.push(accountAddress);

        const response = await user.save();

        res.status(200).json({ message: "XRPL account added successfully", user: response, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};



exports.getxrplBalance = async (req, res) => {
    try {
        const { account } = req.body
        const data = await client.send({
            id: 1,
            command: "account_info",
            account: account,
            strict: true // Ensures strict validation of the account
        });
        if (data.account_data.Balance) {
            res.json({
                status: "success",
                Balance: Number(data.account_data.Balance) / 1000000 - 10 - 2 * data.account_data.OwnerCount
            })
        } else {
            res.json({
                status: "error",
                Bmessage: "not a valid address"
            })
        }
    } catch (err) {
        res.status(500).json({ error: err.message });
    }

}


exports.getTokens = async (req, res) => {
    try {
        let net = process.env.xrpltestnet;
        const clients = new xrpl.Client(net);
        await clients.connect(); // Add this line to connect to the XRPL client

        const { raddress } = req.body;
        const nfts = await clients.request({
            method: "account_nfts",
            account: raddress
        });
        const accountNfts = nfts.result.account_nfts;
        const uriList = [];
        accountNfts.forEach(nft => {
            const uri = Buffer.from(nft.URI, 'hex').toString('utf-8');
            uriList.push(uri);
        });

        console.log("NFT URI List:", uriList);
        res.json({ status: "success", uriList: uriList });
    } catch (error) {
        console.error("Error fetching tokens:", error);
        res.status(500).json({ status: "error", message: "Failed to fetch tokens" });
    }
}



async function mintNFT(metadataIpfsHash, seed) {
    const standby_wallet = xrpl.Wallet.fromSeed(seed);
    const client = new xrpl.Client(net);
    try {
        await client.connect();
        const transactionJson = {
            "TransactionType": "NFTokenMint",
            "Account": standby_wallet.classicAddress,
            "URI": xrpl.convertStringToHex(`https://gateway.pinata.cloud/ipfs/${metadataIpfsHash}`),
            "Flags": parseInt(standbyFlags),
            "TransferFee": parseInt(standbyTransferFee),
            "NFTokenTaxon": 0
        };
        const tx = await client.submitAndWait(transactionJson, { wallet: standby_wallet });
        console.log("NFT Minted, Transaction:", tx.result.meta);
        return tx.result.meta
    } catch (error) {
        console.error("Error minting NFT:", error);
    } finally {
        client.disconnect();
    }
}

function getCurrentDateAndTime() {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth() + 1;
    const day = currentDate.getDate();
    const hours = currentDate.getHours();
    const minutes = currentDate.getMinutes();
    const seconds = currentDate.getSeconds();
    const formattedDateTime = `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day} ${hours < 10 ? '0' + hours : hours}:${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
    return formattedDateTime;
}

async function uploadMetadataToPinata(metadata) {
    try {
        const result = await pinata.pinJSONToIPFS(metadata);
        console.log("Metadata uploaded to Pinata:", result);
        return result;
    } catch (error) {
        console.error("Error uploading metadata to Pinata:", error);
        throw new Error("Failed to upload metadata to Pinata");
    }
}

const MakePayment = async (seed, destination, price) => {
    try {
        if (!utils.isValidSeed(seed)) {
            throw new Error("Invalid seed");
        }

        const account = derive.familySeed(seed);

        if (!utils.isValidAddress(destination)) {
            throw new Error("Invalid destination address");
        }

        console.log("Account: ", account);

        const data = await client.send({
            id: 1,
            command: "account_info",
            account: account.address,
            strict: true
        });

        console.log("Account Data: ", data.account_data);

        if (data.error) {
            throw new Error(data.error_message);
        }

        const balance = Number(data.account_data.Balance) / 1000000 - 10 - 2 * data.account_data.OwnerCount;

        console.log("Balance: ", balance);

        if (balance < Number(price) + 10 - 2 * data.account_data.OwnerCount) {
            console.log("Warning: Your balance is low\n");
            throw new Error("Insufficient balance");
        }

        await client.send({
            command: "subscribe",
            accounts: [account.address]
        });

        const { id, signedTransaction } = sign({
            TransactionType: "Payment",
            Account: account.address,
            Destination: destination,
            Amount: String(Number(price) * 1_000_000),
            Sequence: data.account_data.Sequence,
            Fee: String(12),
            LastLedgerSequence: data.ledger_current_index + 2
        }, account);

        console.log("Transaction ID: ", id);
        console.log("Signed Transaction: ", signedTransaction);

        const result = await client.send({
            command: "submit",
            tx_blob: signedTransaction
        });

        console.log("Submitted transaction result: ", result);

        return { transactionid: id, raddress: account.address, testresult: result.engine_result };
    } catch (error) {
        console.error("Error making payment:", error);
        throw error;
    }
};
exports.mintToken = async (req, res) => {
    const { eventName, location, attractionType, username, secretKey, attractionAccount, amount } = req.body;
    const dateTime = getCurrentDateAndTime();
    const termsConditions = "Non-refundable. Valid only on the specified date and time"
    try {
        const { transactionid, raddress, testresult } = await MakePayment(secretKey, attractionAccount, amount);
        console.log("raddress", raddress)
        if (testresult && testresult === "tesSUCCESS") {
            const metadata = {
                ownerAddress: raddress,
                dateTime,
                eventName,
                location,
                termsConditions,
                attractionType,
                username
            };
            // console.log("metadata",metadata)
            const metadataResult = await uploadMetadataToPinata(metadata);
            const mintedNFT = await mintNFT(metadataResult.IpfsHash, secretKey);
            if (mintedNFT?.TransactionResult === "tesSUCCESS") {
                const uploadToDataBase = await NFT.create({
                    ownerAddress: raddress,
                    nftID: mintedNFT.nftoken_id,
                    eventName: eventName,
                    location: location,
                    dateTime: dateTime,
                    termsConditions: termsConditions,
                    attractionType: attractionType,
                    attractionOwner: attractionAccount
                });
                console.log("uploadToDataBase", uploadToDataBase);
                return res.json({ nft: uploadToDataBase, status: "success" });
            } else {
                console.error("Failed Uploading NFT", mintedNFT);
                return res.status(500).json({ status: "error", message: "Failed Uploading NFT" });
            }
        } else {
            console.error("Payment failed", testresult);
            return res.status(500).json({ status: "error", message: "Payment failed" });
        }
    } catch (error) {
        console.error("Error in process:", error);
        return res.json({ status: "error", message: error.message });
    }
};





exports.mintNFTFOrHotel = async (req, res) => {
    const { eventName, location, attractionType, username, secretKey, attractionAccount, amount, selectedStartDate, selectedEndDate } = req.body;
    const dateTime = getCurrentDateAndTime();
    const termsConditions = "Non-refundable. Valid only on the specified date and time"
    try {
        const { transactionid, raddress, testresult } = await MakePayment(secretKey, attractionAccount, amount);
        console.log("raddress", raddress)
        if (testresult && testresult === "tesSUCCESS") {
            const metadata = {
                ownerAddress: raddress,
                dateTime,
                eventName,
                location,
                termsConditions,
                attractionType,
                username,
                selectedStartDate,
                selectedEndDate
            };
            // console.log("metadata",metadata)
            const metadataResult = await uploadMetadataToPinata(metadata);
            const mintedNFT = await mintNFT(metadataResult.IpfsHash, secretKey);
            if (mintedNFT?.TransactionResult === "tesSUCCESS") {
                const uploadToDataBase = await HotelBooking.create({
                    ownerAddress: raddress,
                    nftID: mintedNFT.nftoken_id,
                    eventName: eventName,
                    location: location,
                    dateTime: dateTime,
                    termsConditions: termsConditions,
                    attractionType: attractionType,
                    attractionOwner: attractionAccount,
                    startDate: selectedStartDate,
                    endDate: selectedEndDate
                });
                console.log("uploadToDataBase", uploadToDataBase);
                return res.json({ nft: uploadToDataBase, status: "success" });
            } else {
                console.error("Failed Uploading NFT", mintedNFT);
                return res.status(500).json({ status: "error", message: "Failed Uploading NFT" });
            }
        } else {
            console.error("Payment failed", testresult);
            return res.status(500).json({ status: "error", message: "Payment failed" });
        }
    } catch (error) {
        console.error("Error in process:", error);
        return res.json({ status: "error", message: error.message });
    }
};






exports.getNFTsByOwnerAddress = async (req, res) => {
    try {
        const { ownerAddress } = req.params;
        const nfts = await NFT.find({ ownerAddress });
        if (nfts.length === 0) {
            return res.status(404).json({ message: "No NFTs found for the specified owner address" });
        }
        res.json({ nfts, status: "success" });
    } catch (error) {
        console.error("Error fetching NFTs by owner address:", error);
        res.status(500).json({ error: "Internal server error" });
    }
};



exports.getNFTsByOwnerAddressForHotel = async (req, res) => {
    try {
        const { ownerAddress } = req.params;
        const nfts = await HotelBooking.find({ ownerAddress });
        if (nfts.length === 0) {
            return res.status(404).json({ message: "No NFTs found for the specified owner address" });
        }
        res.json({ nfts, status: "success" });
    } catch (error) {
        console.error("Error fetching NFTs by owner address:", error);
        res.status(500).json({ error: "Internal server error" });
    }
};


exports.getNFTsOFHotelByAttractionOwner = async (req, res) => {
    try {
        const { attractionOwner } = req.params;
        const bookings = await HotelBooking.find({ attractionOwner });
        if (bookings.length === 0) {
            return res.status(404).json({ message: "No bookings found for the specified attraction owner" });
        }
        res.json({ bookings, status: "success" });
    } catch (error) {
        console.error("Error fetching bookings by attraction owner:", error);
        res.status(500).json({ error: "Internal server error" });
    }
};





