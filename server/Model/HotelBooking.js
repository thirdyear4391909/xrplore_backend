// models/bookingModel.js
const mongoose = require('mongoose');

const bookingSchema = new mongoose.Schema({
    ownerAddress: {
        type: String,
        required: true,
    },
    nftID: {
        type: String,
        required: true,
        unique: true,
    },
    eventName: {
        type: String,
        required: true,
    },
    location: {
        type: String,
        required: true,
    },
    dateTime: {
        type: String,
        required: true,
    },
    termsConditions: {
        type: String,
        required: true,
    },
    attractionOwner: {
        type: String,
        required: true,
    },
    startDate: {
        type: Date,
        required: true,
    },
    endDate: {
        type: Date,
        required: true,
    },
    showStatus: {
        type: Boolean,
        default: true,
    },
});

const Booking = mongoose.model('Booking', bookingSchema);

module.exports = Booking;
