const mongoose = require('mongoose');

const nftSchema = new mongoose.Schema({
    ownerAddress: {
        type: String,
        required: true,
    },
    nftID: {
        type: String,
        required: true,
        unique: true,
    },
    eventName: {
        type: String,
        required: true,
    },
    location: {
        type: String,
        required: true,
    },
    dateTime: {
        type: String,
        required: true,
    },
    termsConditions: {
        type: String,
        required: true,
    },
    attractionOwner: {
        type: String,
        required: true,
    },
    
});

const NFT = mongoose.model('NFT', nftSchema);

module.exports = NFT;
