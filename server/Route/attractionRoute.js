const express = require('express')
const attractionController = require('../Controller/atttractionController')
const imageController = require("../Controller/imageController")
const router = express.Router()
router.post(process.env.uploadAttraction,attractionController.uploadAttraction)
router.post(process.env.addhighlight,attractionController.addhighlight)

router.get(process.env.getAllAttractionOfSameType,attractionController.getAllAttractionOfSameType)
router.get(process.env.getAllAttractions,attractionController.getAllAttractions)
router.get(process.env.getAllAttractionsByUserID,attractionController.getAllAttractionsByUserID)



router.put(process.env.addattractionImage,imageController.uploadPhoto,attractionController.addAttractionImage)
router.put(process.env.updateAttractionDetails,attractionController.updateAttractionDetails)


router.post(process.env.addreminderforAttraction,attractionController.addingReminder)
router.put(process.env.deletereminderforAttraction,attractionController.deleteReminder)
router.put(process.env.deletehighlight,attractionController.deleteHighlight)










// router.post('/addhighlight',hotelController.addhighlight)
// router.post('/addreminder',hotelController.addingReminder)

// router.put('/updatehotel',hotelController.updateHotelDetails)
// router.put('/updatehighlight',hotelController.updateHighlight)
// router.put('/updatereminder',hotelController.updateReminder)

// router.put('/deletereminder',hotelController.deleteReminder)
// router.put('/deletehighlight',hotelController.deleteHighlight)


// router.put('/addhotelimage',imageController.uploadPhoto,hotelController.addHotelImage)

// router.put('/deletehotelimage',hotelController.deleteHotelImage)










module.exports = router